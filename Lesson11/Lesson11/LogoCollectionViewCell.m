//
//  LogoCollectionViewCell.m
//  Lesson11
//
//  Created by iOS-School-2 on 29.04.17.
//  Copyright © 2017 iOS-School-2. All rights reserved.
//

#import "LogoCollectionViewCell.h"

@interface LogoCollectionViewCell ()

@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;

@end

@implementation LogoCollectionViewCell

-(void)setLogoModel:(LogoModel *)logoModel {
    self.imageView.image = logoModel.image;
    self.titleLabel.text = logoModel.title;
    
}

-(void)awakeFromNib {
    [super awakeFromNib];
    
    self.imageView.layer.cornerRadius = 64;
}

@end
