//
//  ViewController.m
//  Lesson11
//
//  Created by iOS-School-2 on 29.04.17.
//  Copyright © 2017 iOS-School-2. All rights reserved.
//

#import "ViewController.h"
#import "LogoCollectionViewCell.h"

static NSString * const kCellID = @"LogoCollectionViewCellID";

@interface ViewController () <UICollectionViewDataSource, UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) NSMutableArray *models;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    self.models = [NSMutableArray new];
    
    LogoModel *model = [LogoModel new];
    model.title = @"Lol";
    model.image = [UIImage imageNamed:@"1"];
    [self.models addObject:model];
    
    model = [LogoModel new];
    model.title = @"Kek";
    model.image = [UIImage imageNamed:@"2"];
    [self.models addObject:model];
    
    model = [LogoModel new];
    model.title = @"Lol";
    model.image = [UIImage imageNamed:@"3"];
    [self.models addObject:model];
    
    model = [LogoModel new];
    model.title = @"Kek";
    model.image = [UIImage imageNamed:@"4"];
    [self.models addObject:model];
    
    model = [LogoModel new];
    model.title = @"Lol";
    model.image = [UIImage imageNamed:@"5"];
    [self.models addObject:model];
    
    model = [LogoModel new];
    model.title = @"Kek";
    model.image = [UIImage imageNamed:@"6"];
    [self.models addObject:model];
    
    model = [LogoModel new];
    model.title = @"Kek";
    model.image = [UIImage imageNamed:@"7"];
    [self.models addObject:model];
    
    model = [LogoModel new];
    model.title = @"Lol";
    model.image = [UIImage imageNamed:@"8"];
    [self.models addObject:model];
    
    model = [LogoModel new];
    model.title = @"Kek";
    model.image = [UIImage imageNamed:@"9"];
    [self.models addObject:model];
    
    model = [LogoModel new];
    model.title = @"Lol";
    model.image = [UIImage imageNamed:@"10"];
    [self.models addObject:model];
    
}

#pragma mark - DataSource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.models.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LogoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellID forIndexPath:indexPath];
    
    LogoModel *model = self.models[indexPath.row];
    [cell setLogoModel:model];
    
    return cell;
}


@end
