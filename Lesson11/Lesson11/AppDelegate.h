//
//  AppDelegate.h
//  Lesson11
//
//  Created by iOS-School-2 on 29.04.17.
//  Copyright © 2017 iOS-School-2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

